package com.example.pokeapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class Principal : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)

        val fragmentLogin =fragmentLogin.newInstance("","")

        supportFragmentManager.beginTransaction().replace(R.id.framePrincipal,fragmentLogin).commit()

    }
}
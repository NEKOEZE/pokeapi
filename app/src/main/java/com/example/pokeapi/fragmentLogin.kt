package com.example.pokeapi

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [fragmentLogin.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragmentLogin : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentPrincipal =fragmentPrincipal.newInstance("", "")
        val fragmentManager: FragmentManager? = fragmentManager
        val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()



        val prefs = activity!!.getSharedPreferences("DatosUsuario",0)
        if(prefs.getBoolean("seccion",false))
        {

            transaction.replace(R.id.framePrincipal, fragmentPrincipal, "fragmentPrincipal").commit()

        }
        // Inflate the layout for this fragment
      val view =  inflater.inflate(R.layout.fragment_login, container, false)

        view.btnInciar.setOnClickListener{

            if (!view.txtUsuario.text.toString().equals("123") || !view.txtPass.text.toString().equals("123")){
                Toast.makeText(view.context,"Usuaio o contraseña incorrecto",Toast.LENGTH_SHORT).show()

            }else{
                mediaPlayer = MediaPlayer.create(view.context, R.raw.pika)
                mediaPlayer.start()

                val editor = prefs.edit()
                editor.putBoolean("seccion",true );
                editor.commit()


                transaction.replace(R.id.framePrincipal, fragmentPrincipal, "fragmentPrincipal").commit()

            }
                }
        return view;
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragmentLogin.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragmentLogin().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
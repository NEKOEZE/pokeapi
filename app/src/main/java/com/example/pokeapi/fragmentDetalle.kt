package com.example.pokeapi

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokeapi.adapter.ListaDetalle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detalle.view.*
import kotlinx.android.synthetic.main.fragment_principal.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"


/**
 * A simple [Fragment] subclass.
 * Use the [fragmentDetalle.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragmentDetalle : Fragment() {
    // TODO: Rename and change types of parameters
    private var paramURL: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramURL = it.getString(ARG_PARAM1)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       var view= inflater.inflate(R.layout.fragment_detalle, container, false)
        view.recyclerHabilidad.layoutManager= LinearLayoutManager(context)
        getPokemonDetalle(view)

        view.linearEsperaDetalle.visibility= View.VISIBLE

        view.imgBack.setOnClickListener{

            val fragmentManager = fragmentManager
            fragmentManager!!.popBackStack()

        }
        return view
    }

   fun setImagen(view: View, url: String){
       Picasso.get().load(url).into(view.imgDetalle)

   }

    fun getPokemonDetalle(view: View){

        Log.e("hola",paramURL.toString())

        val api = Retrofit.Builder()
                .baseUrl(paramURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiRequests::class.java)

        GlobalScope.launch {
            Dispatchers.IO
            val response = api.getPokemonDetalle().awaitResponse()
            if (response.isSuccessful) {
                val data = response.body()!!


                withContext(Dispatchers.Main) {
                    val adapter = ListaDetalle(data)
                    view.txtNombreDetalle.text=  data.name
                    view.linearEsperaDetalle.visibility= View.GONE
                    setImagen(view, data.sprites.front_default)
                    view.recyclerHabilidad.adapter = adapter
                }
            }
        }


    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragmentDetalle.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            fragmentDetalle().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)

                }
            }
    }
}
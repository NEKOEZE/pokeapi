package com.example.pokeapi.api

data class AbilityX(
    val name: String,
    val url: String
)
package com.example.pokeapi.api

data class GenerationVii(
    val icons: Icons,
    val ultrasunultramoon: UltraSunUltraMoon
)
package com.example.pokeapi.api

data class GenerationVi(
    val omegarubyalphasapphire: OmegarubyAlphasapphire,
    val xy: XY
)
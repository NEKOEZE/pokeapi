package com.example.pokeapi.api

data class GenerationIv(
    val diamondpearl: DiamondPearl,
    val heartgoldsoulsilver: HeartgoldSoulsilver,
    val platinum: Platinum
)
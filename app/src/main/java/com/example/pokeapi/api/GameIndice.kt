package com.example.pokeapi.api

data class GameIndice(
    val game_index: Int,
    val version: Version
)
package com.example.pokeapi.api

data class GenerationI(
    val redblue: RedBlue,
    val yellow: Yellow
)
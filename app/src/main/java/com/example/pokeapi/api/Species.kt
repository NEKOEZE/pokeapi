package com.example.pokeapi.api

data class Species(
    val name: String,
    val url: String
)
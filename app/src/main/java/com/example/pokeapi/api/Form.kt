package com.example.pokeapi.api

data class Form(
    val name: String,
    val url: String
)
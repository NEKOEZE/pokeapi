package com.example.pokeapi.api

data class pokemonJson(
    val count: Int,
    val next: String,
    val previous: Any,
    val results: List<Result>
)
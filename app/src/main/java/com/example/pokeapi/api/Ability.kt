package com.example.pokeapi.api

data class Ability(
    val ability: AbilityX,
    val is_hidden: Boolean,
    val slot: Int
)
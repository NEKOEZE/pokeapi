package com.example.pokeapi.api

data class Type(
    val slot: Int,
    val type: TypeX
)
package com.example.pokeapi.api

data class TypeX(
    val name: String,
    val url: String
)
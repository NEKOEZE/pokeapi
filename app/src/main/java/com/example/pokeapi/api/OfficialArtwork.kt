package com.example.pokeapi.api

data class OfficialArtwork(
    val front_default: String
)
package com.example.pokeapi.api

data class Emerald(
    val front_default: String,
    val front_shiny: String
)
package com.example.pokeapi.api

data class Version(
    val name: String,
    val url: String
)
package com.example.pokeapi.api

data class StatX(
    val name: String,
    val url: String
)
package com.example.pokeapi.api

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)
package com.example.pokeapi.api

data class GenerationIii(
    val emerald: Emerald,
    val fireredleafgreen: FireredLeafgreen,
    val rubysapphire: RubySapphire
)
package com.example.pokeapi.api

data class Result(
    val name: String,
    val url: String
)
package com.example.pokeapi.api

data class GenerationIi(
    val crystal: Crystal,
    val gold: Gold,
    val silver: Silver
)
package com.example.pokeapi.api

data class MoveX(
    val name: String,
    val url: String
)
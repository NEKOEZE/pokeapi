package com.example.pokeapi.api

data class VersionGroup(
    val name: String,
    val url: String
)
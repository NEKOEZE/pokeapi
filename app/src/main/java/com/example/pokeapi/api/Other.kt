package com.example.pokeapi.api

data class Other(
    val dream_world: DreamWorld,
    val officialartwork: OfficialArtwork
)
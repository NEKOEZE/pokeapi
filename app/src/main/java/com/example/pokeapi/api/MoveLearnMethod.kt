package com.example.pokeapi.api

data class MoveLearnMethod(
    val name: String,
    val url: String
)
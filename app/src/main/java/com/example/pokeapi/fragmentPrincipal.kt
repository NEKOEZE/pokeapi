package com.example.pokeapi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokeapi.adapter.ListaNombre
import kotlinx.android.synthetic.main.fragment_principal.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
const val BASE_URL="https://pokeapi.co/api/v2/"
/**
 * A simple [Fragment] subclass.
 * Use the [fragmentPrincipal.newInstance] factory method to
 * create an instance of this fragment.
 */
class fragmentPrincipal : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_principal, container, false)


        view.recyclerPokemon.layoutManager=LinearLayoutManager(context)

        view.linearEspera.visibility= View.VISIBLE

        view.imgCloseSeccion.setOnClickListener{
            val prefs = activity!!.getSharedPreferences("DatosUsuario",0)
            val editor = prefs.edit()
            editor.putBoolean("seccion",false );
            editor.commit()

            val fragmentLogin = fragmentLogin.newInstance("","")

            val fragmentManager: FragmentManager? =
                (view.context as AppCompatActivity).supportFragmentManager
            val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()

            transaction.replace(R.id.framePrincipal, fragmentLogin).addToBackStack("fragmentLogin").commit()

        }
        view.imgClose.setOnClickListener{
            getActivity()!!.moveTaskToBack(true);
        }
        getPokemon(view)
        return view

    }

    fun getPokemon(view: View){


            val api =Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiRequests::class.java)

            GlobalScope.launch {
                Dispatchers.IO
                val response = api.getPokemon().awaitResponse()
                if (response.isSuccessful) {
                    val data = response.body()!!


                    withContext(Dispatchers.Main) {
                        val adapter = ListaNombre(data)
                        view.recyclerPokemon.adapter = adapter
                        view.linearEspera.visibility= View.GONE
                    }
                }
            }


    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment fragmentPrincipal.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            fragmentPrincipal().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
package com.example.pokeapi

import com.example.pokeapi.api.detalleJSon
import com.example.pokeapi.api.pokemonJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRequests {

    @GET("pokemon?limit=2000")
    fun getPokemon(): Call<pokemonJson>


    @GET(".")
    fun getPokemonDetalle(): Call<detalleJSon>
}
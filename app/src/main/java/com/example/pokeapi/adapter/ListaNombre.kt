package com.example.pokeapi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.example.pokeapi.R
import com.example.pokeapi.api.pokemonJson
import com.example.pokeapi.fragmentDetalle
import com.example.pokeapi.fragmentPrincipal
import kotlinx.android.synthetic.main.item_row_nombre_pokemon.view.*


class ListaNombre(val pokemonJson: pokemonJson):RecyclerView.Adapter<ListaNombre.Holder>() {

    class Holder(val view: View):RecyclerView.ViewHolder(view){
        fun render(pokemonJson: pokemonJson, position: Int){
            view.txtNombre.text=pokemonJson.results.get(position).name


            view.linearNombre.setOnClickListener{
                val fragmentDetalle = fragmentDetalle.newInstance(pokemonJson.results.get(position).url)

                val fragmentManager: FragmentManager? =
                    (view.context as AppCompatActivity).supportFragmentManager
                val transaction: FragmentTransaction = fragmentManager!!.beginTransaction()

                transaction.replace(R.id.framePrincipal, fragmentDetalle).addToBackStack("fragmentPrincipal").commit()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
       val layoutInflater= LayoutInflater.from(parent.context)
        return Holder(layoutInflater.inflate(R.layout.item_row_nombre_pokemon, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.render(pokemonJson, position)
    }

    override fun getItemCount(): Int {
        return pokemonJson.results.size
    }
}




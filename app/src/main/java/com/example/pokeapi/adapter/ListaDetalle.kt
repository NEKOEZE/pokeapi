package com.example.pokeapi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokeapi.R
import com.example.pokeapi.api.detalleJSon
import kotlinx.android.synthetic.main.item_row_habilidad.view.*

class ListaDetalle(val detalleJSon: detalleJSon): RecyclerView.Adapter<ListaDetalle.Holder>() {

    class Holder(val view: View):RecyclerView.ViewHolder(view) {
        fun render(detalleJSon: detalleJSon, position: Int) {
            view.txtNombre.text = detalleJSon.abilities.get(position).ability.name
            view.linearNombre.setOnClickListener {

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val layoutInflater= LayoutInflater.from(parent.context)
        return Holder(layoutInflater.inflate(R.layout.item_row_habilidad, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.render(detalleJSon, position)
    }

    override fun getItemCount(): Int {
        return detalleJSon.abilities.size
    }


}

